define(['jquery'], function ($) {
  'use strict';

  return function () {
    var self = this;

    this.callbacks = {
      render: function () {
        self.render_template({
          body: '',
          caption: {
            class_name: 'widget-caption-unique-class-name'
          },
          render: '<div class="widget-body-unique-class-name">Test widget</div>'
        });
        let term_A = $( "input[name='CFV[1451087]']" );
        let term_B = $( "input[name='CFV[1451089]']" );
        let sum = $( "input[name='CFV[1451091]']" );

        function summa() {
          return Number(term_A.val()) + Number(term_B.val());
        }

        term_A.change(function() {
          sum.val(summa());
        });
        term_B.change(function() {
          sum.val(summa());
        });
        sum.change(function() {
          sum.val(summa());
        });
        return true;
      },

      init: function () {
        return true;
      },

      bind_actions: function () {
        return true;
      },

      settings: function () {
        return true;
      },

      onSave: function () {
        return true;
      }
    };
  };
});
